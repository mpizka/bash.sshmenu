# sshmenu

`sshmenu` is a convenient CLI tool for managing ssh connections as a
simple textfile and using them via a CLI friendly menu.

It is accompanied by `sctpmenu` and `sftpmenu`, which do the same for scp and
sftp, respectively.


# Installation

Make sure the `dialog` package is installed (eg. `apt-get install dialog`)

Copy the `sshmenu`, `sctpmenu`, `sftpmenu` files to a convenient location
listed in the `PATH` envvar, make sure they are executeable:

```sh
$ chmod 755 sshmenu
$ chmod 755 sctpmenu
$ chmod 755 sftpmenu
```

Create a list-file and either edit the `LISTFILE` variable in both files, or
export SSHMENULISTFILE envvar to your environment by adding the following to
your `.bashrc`:

```sh
export SSHMENULISTFILE="/path/to/your/listfile"
```


# Listfile

The listfile cosists of one line per connection. Each line follows the same syntax:

```
listname:[user@]host:[arguments]
```

`listname` is the name of the entry in the dialog-menu. `host` is the ip or
domain you try to connect to. `user` is your user on the remote machine, which
may be required if it differs from your local user. `arguments` are additional
arguments you want to pass to `ssh` or `scp` for this connection. The most
common use case for this is to set a keyfile to be used for authentication.

For an example see `sshmenulist`.


# Usage

* `sshmenu` : run ssh to the server from the list

* `sftpmenu` : run sftp to the server from the list

* `scpmenu SRC... DST` : copy the files in SRC on local, to DST on remote

* `scpmenu -f SRC... DST` : copy the files in SRC on remote, to DST on local

* `scpmenu -r SRC... DST` : copy recursively (directories)


# License

See the information under [LICENSE](./LICENSE)
